using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject hammer;
    [SerializeField] private GameObject sword;
    [SerializeField] private GameObject axe;

    private int x = 1;

    // Update is called once per frame
    void Update()
    {
        OnNumberDonw();
        OnScrollWheel();
    }

    private void OnNumberDonw()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                animator.SetTrigger("isAttack");

            if (Input.GetKeyDown(KeyCode.Alpha2))
                animator.SetTrigger("isEntry");

            if (Input.GetKeyDown(KeyCode.Alpha3))
                animator.SetTrigger("isTaunt");

            if (Input.GetKeyDown(KeyCode.Alpha4))
                animator.SetTrigger("isJump");

            if (Input.GetKeyDown(KeyCode.Alpha5))
                animator.SetTrigger("isHipHop");
        }
    }

    private void OnScrollWheel()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {

            if (x == 1)
            {
                x++;

                hammer.SetActive(false);
                sword.SetActive(true);
                axe.SetActive(false);
            }
            else if (x == 2)
            {
                x++;

                hammer.SetActive(false);
                sword.SetActive(false);
                axe.SetActive(true);
            }
            else if (x == 3)
            {
                x = 1;

                hammer.SetActive(true);
                sword.SetActive(false);
                axe.SetActive(false);
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            if (x == 1)
            {
                x = 3;

                hammer.SetActive(false);
                sword.SetActive(false);
                axe.SetActive(true);
            }
            else if (x == 2)
            {
                x--;

                hammer.SetActive(true);
                sword.SetActive(false);
                axe.SetActive(false);
            }
            else if (x == 3)
            {
                x--;

                hammer.SetActive(false);
                sword.SetActive(true);
                axe.SetActive(false);
            }
        }
    }
}
